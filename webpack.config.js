"use strict";

const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = {
	context: __dirname,
	devtool: 'source-map',
	separateStylesheet: true,
	debug: true,
	entry: {
		app: ['webpack/hot/dev-server', './src'],
	},
	output: {
		path: './dist',
		filename: 'bundle.js',
	},
	module: {
		loaders: [
			{ test: /\.js$/, loader: 'babel?presets[]=es2015', exclude: /node_modules/ },
			{ test: /\.html$/, loader: 'html' },
			{ test: /\.css/, loader: 'style!css' },
			{ test: /\.scss/, loader: 'style!css!sass' },
			{ test: /\.(jpg|png)$/, loader: 'url?limit=10000' },
			{ test: /\.woff$/, loader: 'url?limit=10000&mimetype=application/font-woff', },
			{ test: /\.woff2$/, loader: 'url?limit=10000&mimetype=application/font-woff', },
			{ test: /\.ttf$/, loader: 'url?limit=10000&mimetype=application/octet-stream', },
			{ test: /\.eot$/, loader: 'file', },
			{ test: /\.svg$/, loader: 'url?limit=10000&mimetype=image/svg+xml', },
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			template: 'src/index.html',
		}),
	],
	fileLoader: {
		/*
		 * We don't want to comtaminate the root of the served files more than necessary.
		 * Therefore we serve these files from a separate folder, assets.
		 */
		name: 'assets/[hash].[ext]',
	}
};

module.exports = config;
