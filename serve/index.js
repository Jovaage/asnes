"use strict";

// Module dependencies
const express = require('express');
const chalk = require('chalk');
const morgan = require('morgan');

const app = express();

// Express configuration
app.set('name', "Åsnes");
app.set('port', process.env.PORT || 3000);

app.use(morgan('dev'));

// Adds static files
app.use(express.static('./dist'));

// Start Express server
app.listen(app.get('port'))
	.on('listening', () => {
		console.log(`${chalk.bold.blue(app.get('name'))} listening on port ${chalk.bold.blue(app.get('port'))}`);
	})
	.on('error', error => {
		console.error(`${chalk.bold.red('ExpressError:')} ${error.syscall} ${error.code} ${error.address}:${error.port}`);
		process.exit(1);
	});

