"use strict";

import Slideshow from './slideshow';

import './style/index.scss';

document.addEventListener('DOMContentLoaded', () => {
	const slideshowElement = document.getElementById('slideshow');
	const classes = {
		paginator: 'slideshow__paginator',
		paginatorItem: 'slideshow__paginator-item',
		current: 'slideshow__paginator-item--current',
		previous: 'slideshow__previous',
		next: 'slideshow__next',
	};
	const animation = {
		duration: 1000,
		timing: 'ease',
	};

	const slideshow = new Slideshow({ slideshowElement, classes, animation });
})