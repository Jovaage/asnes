"use strict";

export default class Slideshow {
	constructor({ slideshowElement, classes, animation }) {
		this.slideshowElement = slideshowElement;
		this.classes = classes;
		this.animation = animation;

		this.state = 0;

		const slides = this.slides = Array.from(slideshowElement.children);

		slides.forEach((slide, i) => {
			const img = slide.querySelector('img');
			const imgSrc = img.getAttribute('src');
			img.remove();

			slide.style.backgroundImage = `url(${imgSrc})`;
			slide.style.transition = `left ${animation.duration/1000}s ${animation.timing}`;

			if (i !== 0) {
				slide.style.left = '100%';
			}

			const paginatorElement = document.createElement('div');
			paginatorElement.classList.add(classes.paginator);
			slide.children[0].appendChild(paginatorElement);

			slides.forEach((slide, j) => {
				const paginatorItemElement = document.createElement('span');
				paginatorItemElement.classList.add(classes.paginatorItem);
				if (i === j) {
					paginatorItemElement.classList.add(classes.current);
				} else {
					paginatorItemElement.addEventListener('click', () => { this.goto(j) })
				}
				paginatorElement.appendChild(paginatorItemElement);
			});
		});

		const previousElement = this.previousElement = document.createElement('div');
		previousElement.classList.add(classes.previous);
		previousElement.addEventListener('click', () => { this.previous() });
		slideshowElement.insertBefore(previousElement, slideshowElement.firstChild);

		const nextElement = this.nextElement = document.createElement('div');
		nextElement.classList.add(classes.next);
		nextElement.addEventListener('click', () => { this.next() });
		slideshowElement.appendChild(nextElement);
	}

	goto(newState) {
		if (this.timeout) return;

		const oldState = this.state;

		this.slides.forEach((slide, i) => {
			if (i < newState) {
				slide.style.left = '-100%';
				if (i === oldState) {
					slide.style.zIndex = '-1';
				} else {
					slide.style.zIndex = '-2';
				}
			} else if (i === newState) {
				slide.style.left = '0';
				slide.style.zIndex = '0';
			} else if (i > newState) {
				slide.style.left = '100%';
				if (i === oldState) {
					slide.style.zIndex = '-1';
				} else {
					slide.style.zIndex = '-2';
				}
			}
		});

		this.state = newState;

		this.timeout = true;
		setTimeout(() => {
			this.timeout = false;
		}, this.animation.duration);
	}

	previous() {
		const slides = this.slides.length;

		this.goto((this.state - 1 + slides) % slides);
	}

	next() {
		const slides = this.slides.length;

		this.goto((this.state + 1 + slides) % slides);
	}
};
