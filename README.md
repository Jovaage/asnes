#Åsnes

## Build setup

##### Install dependencies  
`yarn` or `npm install`

##### Build application
`yarn build` or `npm run build`

##### Serve application
`yarn serve` or `npm run serve`  
or serve files in `dist` folder by other means